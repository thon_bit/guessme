﻿using UnityEngine;
using System.Collections;

public class AudioSystem : MonoBehaviour {

	public AudioSource audioSource;
	public AudioClip[] audioClips;


	public static AudioSystem instance{ get; private set;}
	// Use this for initialization
	void Awake () {
		instance = this;
	}

	public void PlayAudio(int index){
		try{
			audioSource.clip = audioClips [index];
		}catch(System.Exception ex){
			audioSource.clip = audioClips [0];
		}finally{
			audioSource.Play ();
		}
	}

	public void PlayAudio(){
		audioSource.clip = audioClips [Random.Range (0, audioClips.Length)];
		audioSource.Play ();
	}
}
