﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BoxItem : MonoBehaviour {

	private Color32 defaultColor;
	private Color32 targetColor;
	private float timeToChangeColor;
	private Vector2 defaultScale;
	private int indexSoundEffect;

	[SerializeField] private Image thisImage;

	public RectTransform rectTrans = null;

	public int IdKey { get; set;}

	public void HightLight(float time){
		timeToChangeColor = time;
		ChangeColorToTarget ();

		AudioSystem.instance.PlayAudio (indexSoundEffect);
	}

	public void Init(){
		defaultColor = thisImage.color;
		defaultScale = rectTrans.localScale;
		targetColor = new Color32 ((byte)Random.Range (10, 200), (byte)Random.Range (10, 200), (byte)Random.Range (10, 200),255);
		indexSoundEffect = Random.Range (0, AudioSystem.instance.audioClips.Length);
	}

	public void GuessMe(){
		if (GamePlaySystem.instance.SendGuessingBox (IdKey)) {
			HightLight (.5f);
			AudioSystem.instance.PlayAudio (indexSoundEffect);
		}
	}

	public void ScaleAnimation(float time){
		ItweenValueTo (gameObject,
			new Vector2(.2f,.2f),
			Vector2.one,
			time,
			iTween.EaseType.easeInQuad,
			"ChangeBoxScale"
		);
	}

	private void ChangeBoxScale(Vector2 scale){
		rectTrans.localScale = scale;
	}

	private void ChangeColorToDefault(){
		ItweenValueTo (gameObject,
			(Color)targetColor,
			(Color)defaultColor,
			timeToChangeColor/2f,
			iTween.EaseType.linear,
			"ChangeBoxColor"
		);
	}

	private void ChangeColorToTarget(){
		ItweenValueTo (gameObject,
			(Color)defaultColor,
			(Color)targetColor,
			timeToChangeColor/2f,
			iTween.EaseType.linear,
			"ChangeBoxColor",
			"ChangeColorToDefault"
		);
	}

	private void ChangeBoxColor(Color color){
		thisImage.color = (Color32)color;
	}

	private void ItweenValueTo(GameObject gO,System.Object fromV,System.Object toV,float time
		,iTween.EaseType easeType,string updateMethod,string completeMethod = null){

		Hashtable hash = iTween.Hash (
			"from", fromV,
			"to", toV,
			"time", time,
			"easetype", easeType,
			"onupdate", updateMethod
		);
		if (completeMethod != null)
			hash.Add ("oncomplete", completeMethod);
		iTween.ValueTo (gO, hash);
	}

}