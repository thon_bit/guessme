﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BoxRearrange : MonoBehaviour {

	public Vector2 initPos;
	public int numberOfRandom;
	public int row;
	public int col;
	public float gap;
	public GameObject boxPrefab;
	public Transform parent;

	public Queue<int> randomBoxesToGuess { get; private set;}

	public void RearrangeBox(List<BoxItem> list){
		Vector2 boxPos = initPos;
		int order = 1;
		for (int i = 0; i < col; i++) {
			for (int j = 0; j < row; j++) {
				BoxItem box = ((GameObject)Instantiate (boxPrefab)).GetComponent<BoxItem>();
				box.rectTrans.SetParent(parent);
				box.rectTrans.localPosition = boxPos;
				box.rectTrans.localScale = Vector2.zero;
				box.rectTrans.sizeDelta = new Vector2 (150, 150);
				boxPos.x += gap;
				box.name = "boxItem";
				box.IdKey = order;
				box.Init ();

				list.Add (box);
				order++;
			}

			boxPos.y -= gap;
			boxPos.x = initPos.x;
		}

		RandomGuessingBox (col*row);
	}


	public void ResetRearrangeBox(){

	}

	private void RandomGuessingBox(int maxCount){
		randomBoxesToGuess = new Queue<int> ();
		for (int i = 0; i < numberOfRandom; i++) {
			int elementOrder = Random.Range (1, maxCount + 1);
			randomBoxesToGuess.Enqueue (elementOrder);
		}
	}
	
}
