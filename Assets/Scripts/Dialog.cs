﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Dialog : MonoBehaviour {
	public event System.Action OnCloseDialog;


	public Text txtContent;
	public Text txtButton;
	public GameObject thisPanel;

	void Start(){
		thisPanel.SetActive (false);
	}

	public void Correct(){
		OpenDialog ();
		txtContent.text = "Correct!!!";
		txtButton.text = "Next";
	}

	public void InCorrect(){
		OpenDialog ();
		txtContent.text = "InCorrect!!!";
		txtButton.text = "Try Again";
	}

	private void OpenDialog(){
		thisPanel.SetActive (true);
	}

	public void CloseDialog(){
		thisPanel.SetActive (false);
		if (OnCloseDialog != null)
			OnCloseDialog ();
	}
		
}
