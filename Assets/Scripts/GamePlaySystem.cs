﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GamePlaySystem : MonoBehaviour {

	public float timeForEachBox;
	public float timeDisplay;
	public BoxRearrange boxRearrange;
	public Dialog dialog;
	public Text txtTotalGuess;
	public Text txtRemainingGuess;

	private int numberGuessing;
	private bool isCorrect;

	public List<BoxItem> boxList{ get; private set;}
	public static GamePlaySystem instance { get; private set;}

	void Awake(){
		instance = this;
	}

	// Use this for initialization
	void Start () {
		Init (() => {
			boxRearrange.RearrangeBox (boxList);
		});
	}

	void OnEnable(){
		dialog.OnCloseDialog += ResetGame;
	}

	void OnDisable(){

		dialog.OnCloseDialog -= ResetGame;
	}

	private void ResetGame(){
		Init (() => {
			boxRearrange.ResetRearrangeBox();
		});
	}

	private void Init(System.Action boxCallBack){
		isCorrect = true;
		boxList = new List<BoxItem> ();
		boxCallBack ();
		StartCoroutine (DisplayAllBoxes ());
	}

	private IEnumerator DisplayAllBoxes(){
		float timePerEach = timeDisplay / boxList.Count;
		foreach(BoxItem item in boxList){
			item.ScaleAnimation (timePerEach);
			yield return new WaitForSeconds (timePerEach);
		}

		yield return new WaitForSeconds (1);
		StartCoroutine (DisplayGuessingBoxes ());
	}

	private IEnumerator DisplayGuessingBoxes(){
		int[] orderArr = boxRearrange.randomBoxesToGuess.ToArray ();
		numberGuessing = orderArr.Length;
		txtRemainingGuess.text = "Remain Guessing: " + numberGuessing;
		txtTotalGuess.text = "Total Guessing: " + numberGuessing;

		for (int i = 0; i < numberGuessing; i++) {
			boxList [orderArr [i] - 1].HightLight (timeForEachBox);
			yield return new WaitForSeconds (timeForEachBox);
		}
	}

	public bool SendGuessingBox(int IdKey){
		if (numberGuessing > 0) {
			int order = boxRearrange.randomBoxesToGuess.Dequeue ();
			if (IdKey != order)
				isCorrect = false;
			numberGuessing--;
			if (numberGuessing == 0) {
				Result ();
			}

			txtRemainingGuess.text = "Remain Guessing: " + numberGuessing;
			return true;
		}
		return false;
	}

	private void Result(){
		if (isCorrect) {
			dialog.Correct ();
		} else {
			dialog.InCorrect ();
		}
	}

	private void UpdateRemainNumberGuess(){
		txtRemainingGuess.text = "Remain Guessing: " + numberGuessing;
	}
}
